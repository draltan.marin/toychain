import fs from 'fs';
import minimist from 'minimist';
import Transaction from './lib/transaction';

// read config file as specified in cli options
let args = minimist(process.argv.slice(2)); // ignore node and filename

let keyFile = args.key;
let key = JSON.parse(fs.readFileSync(keyFile, 'utf8'));

key.priv = Buffer.from(key.priv, 'hex');
key.pub = Buffer.from(key.pub, 'hex');

let {from, to, number, value, payload} = args;

number = parseInt(number);
value = parseInt(value);
from = Buffer.from(from, 'hex');
to = Buffer.from(to, 'hex');

let transaction = new Transaction({from, to, value, number, payload}, key.priv);
process.stdout.write(`${transaction.toString('hex')}\n`);
