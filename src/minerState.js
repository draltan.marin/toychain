import config from './config';
import Miner from './lib/mine';
import BlockData from './lib/blockData';
import {node} from './p2pHandlers';
import chain from './chain';
import logger from './logger';

let miner = new Miner(config.chain);

let pendingTransactions = [];
let currentBlock;

export const addBlock = (block) => {
  miner.stop();
  let acceptance = chain.acceptBlock(block);
  // remove transactions in current chain;
  pendingTransactions = pendingTransactions.filter(
    transaction => !chain.hasTransaction(transaction)
  );
  requiresMining();
  return acceptance;
};

export const addTransaction = (transaction, done) => {
  // validate transaction before pushing to pending
  let {balance, lastTransactionNumber} = chain.getState();
  pendingTransactions.forEach(tr => {
    chain.validateTransaction(
      tr,
      false, // isGenesis ?
      balance,
      lastTransactionNumber,
      (err, result) => {
        if (err) logger.error('Pending transaction error ?!', err);
        balance = result.balance;
        lastTransactionNumber = result.lastTransactionNumber;
      }
    );
  });

  chain.validateTransaction(
    transaction,
    false, // isGenesis ?
    balance,
    lastTransactionNumber,
    (err, result) => {
      if (err) {
        logger.info(`Transaction refused ${transaction.hash.toString('hex')}`);
        return done(err);
      }
      balance = result.balance;
      lastTransactionNumber = result.lastTransactionNumber;
      pendingTransactions.push(transaction);
      logger.info(`Transaction pending ${transaction.hash.toString('hex')}`);
      requiresMining();
      return done();
    }
  );
};

const requiresMining = () => {
  if (pendingTransactions.length > 0) {
    let data = new BlockData();

    pendingTransactions.forEach(tr => {
      data.addTransaction(tr);
    });

    currentBlock = chain.createBlock(data);
    miner.setData(currentBlock.getMiningData());

    if(!miner.isRunning) {
      logger.info('Miner: started');
      miner.start()
        .then(result => {
          currentBlock.setNonce(result.nonce);
          logger.info(`Miner: Block found ${currentBlock.hash.toString('hex')}`);
          let {accepted} = addBlock(currentBlock);
          if (accepted && accepted.length > 0) {
            node.broadCast({
              type: 'block',
              data: currentBlock.toObject('hex'),
            });
          }
        })
        .catch(() => logger.info('Miner: stopped'));
    }
  }
};
