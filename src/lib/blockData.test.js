import BlockData from './blockData';
import Transaction from './transaction';
import {createKey} from './keys';

let alice = createKey();
let bob = createKey();

let data = new BlockData();
let tr1 = new Transaction({
  number: 0,
  from: alice.pub,
  to: bob.pub,
  value: 100,
  payload: 'transaction 1',
}, alice.priv);

let tr2 = new Transaction({
  number: 1,
  from: alice.pub,
  to: bob.pub,
  value: 100,
  payload: 'transaction 2',
}, alice.priv);

let tr3 = new Transaction({
  number: 2,
  from: alice.pub,
  to: bob.pub,
  value: 100,
  payload: 'transaction 3',
}, alice.priv);

data.addTransaction(tr1);
data.addTransaction(tr2);
data.addTransaction(tr3);

test('should add transactions', () => {
  let data1 = new BlockData();
  expect(data1.transactions).toHaveLength(0);
  let tr = new Transaction({
    number: 0,
    from: alice.pub,
    to: bob.pub,
    value: 100,
  }, alice.priv);
  data1.addTransaction(tr);
  expect(data1.transactions).toHaveLength(1);
  expect(data1.transactions[0]).toBe(tr);
  expect(data1.index[tr.computeHash().toString('hex')]).toBe(0);
});

test('should avoid adding same transaction twice', () => {
  let tr = new Transaction({
    number: 0,
    from: alice.pub,
    to: bob.pub,
    value: 100,
  }, alice.priv);
  expect(() => data.add(tr)).toThrow();
});

test('should find transaction by hash', () => {
  let tr = data.getTransaction(tr2.computeHash());
  expect(tr.number).toBe(1);
  expect(tr).toBe(tr2);
  expect(tr).not.toBe(tr3);
  expect(tr).not.toBe(tr1);
});

test('should preserve data for hash calculation after encode and decode', () => {
  let encodings = ['base64', 'hex'];
  encodings.forEach(encoding => {
    let mining1 = data.getMiningData();
    let mining2 = BlockData.fromObject(data.toObject(encoding), encoding).getMiningData();
    expect(Buffer.compare(mining1, mining2)).toBe(0);
  });
});
