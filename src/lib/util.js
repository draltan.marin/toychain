import Decoder from 'string_decoder';

export const numToBuffer = (num) => {
  let hex = (num).toString(16);
  return (hex.length % 2 !== 0) ?
    Buffer.from(`0${hex}`, 'hex') :
    Buffer.from(hex, 'hex') ;
};

export const utf8 = (buffer) => (new Decoder.StringDecoder('utf8')).end(buffer);
