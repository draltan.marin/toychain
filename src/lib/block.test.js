import Block from './block';
import BlockData from './blockData';
import Transaction from './transaction';
import {createKey} from './keys';

let alice = createKey();
let bob = createKey();
let data1 = new BlockData();

data1.addTransaction(new Transaction({
  number: 0,
  from: alice.pub,
  to: bob.pub,
  value: 100,
}, alice.priv));

data1.addTransaction(new Transaction({
  number: 1,
  from: alice.pub,
  to: bob.pub,
  value: 100,
}, alice.priv));

let data2 = new BlockData();

data2.addTransaction(new Transaction({
  number: 0,
  from: bob.pub,
  to: alice.pub,
  value: 100,
}, bob.priv));

data2.addTransaction(new Transaction({
  number: 1,
  from: bob.pub,
  to: alice.pub,
  value: 100,
}, bob.priv));


test('should create a genesis block if no previous block', () => {
  let block = new Block(data1);
  expect(block.number).toBe(0);
  expect(block.previous).toHaveLength(0);
  expect(block.isGenesis()).toBe(true);
});

test('should increment number if created from previous block', () => {
  let prev = new Block(data1);
  for (let i = 0; i < 10; i++) {
    let block = new Block(data1, prev);
    expect(block.number).toBe(prev.number + 1);
    prev = block;
  }
});

test('should use previous\' hash if created from previous block', () => {
  let prev = new Block(data1);
  for (let i = 0; i < 10; i++) {
    let block = new Block(data1, prev);
    expect(Buffer.compare(block.previous, prev.hash)).toBe(0);
    prev = block;
  }
});

test('should update hash if data changed', () => {
  let block = new Block(data1);
  let h1 = block.hash;
  block.setData(data2);
  let h2 = block.hash;
  expect(Buffer.compare(h1, h2)).not.toBe(0);
});

test('should update hash if nonce changed', () => {
  let block = new Block(data1);
  let h1 = block.hash;
  block.setNonce('changed');
  let h2 = block.hash;
  expect(Buffer.compare(h1, h2)).not.toBe(0);
});

test('should update hash if miner changed', () => {
  let block = new Block(data2, new Block(data1), alice.pub);
  let h1 = block.hash;
  block.setMiner(bob.pub);
  let h2 = block.hash;
  expect(Buffer.compare(h1, h2)).not.toBe(0);
});

test('should change mining data if data changed', () => {
  let block = new Block(data1);
  let mining1 = block.getMiningData();
  block.setData(data2);
  let mining2 = block.getMiningData();
  expect(Buffer.compare(mining1, mining2)).not.toBe(0);
});

test('should ignore setting miner on genesis', () => {
  // Block should not be genesis
  let block = new Block(data1);
  let mining1 = block.getMiningData();
  block.setMiner(bob.pub);
  let mining2 = block.getMiningData();
  expect(Buffer.compare(mining1, mining2)).toBe(0);
  expect(block.minedBy).toHaveLength(0);
});

test('should change mining data if miner changed', () => {
  // Block should not be genesis
  let block = new Block(data2, new Block(data1), alice.pub);
  let mining1 = block.getMiningData();
  block.setMiner(bob.pub);
  let mining2 = block.getMiningData();
  expect(Buffer.compare(mining1, mining2)).not.toBe(0);
});

test('should preserve data for hash calculation after encode and decode', () => {
  let encodings = ['base64', 'hex'];
  let block = new Block(data1);
  encodings.forEach(encoding => {
    let mining1 = block.getMiningData();
    let mining2 = Block.fromObject(block.toObject(encoding), encoding).getMiningData();
    expect(Buffer.compare(mining1, mining2)).toBe(0);
  });
});

test('should preserve hash after encode and decode', () => {
  let encodings = ['base64', 'hex'];
  let block1 = new Block(data1);
  block1.setMiner(alice.pub);
  encodings.forEach(encoding => {
    let block2 = Block.fromObject(block1.toObject(encoding), encoding);
    expect(Buffer.compare(block1.hash, block2.hash)).toBe(0);
  });
});

test('should reject invalid address as miner', () => {
  let block = new Block(data2, new Block(data1), alice.pub);
  expect(() => block.setMiner('toto')).toThrow();
});

test('should check valid miner', () => {
  let block1 = new Block(data1);
  expect(block1.hasValidMiner()).toBe(false);
  block1.minedBy = Buffer.from('toto');
  expect(block1.hasValidMiner()).toBe(false);
  block1.setMiner(alice.pub);
  expect(block1.hasValidMiner()).toBe(false);

  let block2 = new Block(data2, block1);
  expect(block2.hasValidMiner()).toBe(false); // no miner set by default
  block2.minedBy = Buffer.from('toto'); // manually attempt to change miner
  expect(block2.hasValidMiner()).toBe(false);
  block2.setMiner(alice.pub); // set a valid miner
  expect(block2.hasValidMiner()).toBe(true);
  block2.setMiner(); // remove miner
  expect(block2.hasValidMiner()).toBe(false);
});
