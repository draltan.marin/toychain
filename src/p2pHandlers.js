import Node from './lib/p2p';
import Block from './lib/Block';
import Transaction from './lib/transaction';
import {addTransaction, addBlock} from './minerState';
import chain from './chain';
import {isAccepted, isRejected, isPending} from './lib/chain';
import logger from './logger';

const handleIncommingTransaction = (message) => {
  addTransaction(
    Transaction.fromObject(message.data, 'hex'),
    err => {
      if (err) logger.error('Error adding transaction');
    }
  );
};

const handleIncommingBlock = (message, from) => {
  // if pending, request previous
  let block = Block.fromObject(message.data, 'hex');
  logger.info(`Received block from ${from}: ${block.hash.toString('hex')}`);
  let result = addBlock(block);
  if (isPending(result, block)) {
    logger.info(`Missing block, requesting ${block.previous.toString('hex')}`);
    node.to(from, {
      type: 'request_block',
      hash: block.previous.toString('hex'),
    });
  }
  if (isRejected(result, block)){
    if (block.isGenesis()) {
      logger.warn(`Invalid genesis block from ${from}: ${block.hash.toString('hex')}`);
      node.disconnect(from);
    } else {
      logger.warn(`Rejected incomming block from ${from}: ${block.hash.toString('hex')}`);
    }
  }
  if (isAccepted(result, block)) {
    logger.info(`Accepted incomming block from ${from}: ${block.hash.toString('hex')}`);
  }
};

const handleBlockRequest = (message, from) => {
  let requestedBlock = chain.getBlockByHash(Buffer.from(message.hash, 'hex'));
  node.to(from, {
    type: 'block',
    data: requestedBlock.toObject('hex'),
  });
};

const p2pHandler = (message, from) => {
  // console.log(`${from}: ${message}`);
  switch (message.type) {
  case 'transaction':
    handleIncommingTransaction(message, from);
    break;
  case 'block':
    handleIncommingBlock(message, from);
    break;
  case 'request_block':
    handleBlockRequest(message, from);
    break;
  default:
    logger.error(`Unknown message type: ${message.type}`);
  }
};

const welcome = () => ({
  type: 'block',
  data: chain.currentBlock().toObject('hex')
});

// TODO request blocks up until last
export const node = new Node(p2pHandler, welcome, logger);
