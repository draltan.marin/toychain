import * as keys from './keys';

test('should create a valid key', () => {
  let key = keys.createKey();

  expect(key.priv).toBeDefined();
  expect(key.pub).toBeDefined();
});

test('should create the same key from a given private key', () => {
  let key0 = keys.createKey();

  let key1 = keys.createKey(key0.priv);

  expect(Buffer.compare(key0.pub, key1.pub)).toBe(0);
  expect(Buffer.compare(key0.priv, key1.priv)).toBe(0);
});
