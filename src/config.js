import fs from 'fs';
import minimist from 'minimist';
import chainSetup from './chainSetup.json';
import Block from './lib/Block';

// read config file as specified in cli options
let args = minimist(process.argv.slice(2)); // ignore node and filename
let configFile = args.config || 'config.json';
let config = JSON.parse(fs.readFileSync(configFile, 'utf8'));

// read genesis data
let genesisFile = args.genesis || 'genesis.json';
let genesis = JSON.parse(fs.readFileSync(genesisFile, 'utf8'));

config.genesis = Block.fromObject(genesis, 'hex');
config.chain = {
  ...chainSetup,
  minerAddress: Buffer.from(config.minerAddress || '', 'hex')
};

export default config;
