# Toy blockchain

Create a new toy blockchain from scratch (i.e. not by forking an existing one). The chain should have these properties:

- Running on at least 2 pre-defined nodes (IP addresses given in a config file / no additional discovery necessary)
- PoW, with every node a miner
- The difficulty is fixed (i.e., block time is not), each block rewards 100 coins
- ECDSA cryptography; addresses may be pure public keys
- Protect against double spending
- Transactions can have additional data payload, signed by the sender (and there needs to be a way to set it)
- Blocks can have additional data payload (and there needs to be a way to set it)
- Implement a simple REST-like web service API which implements the following services and returns JSON data:
  - Query the block count.
  - Query a single block and return all its transaction data.
  - Query a single transaction and return all its data.
  - (new): Create a transaction from JSON data and accept it for mining

## Usage

Start a node

```
npm run node -- --config config0.json
```

Start a node using a custom genesis block

```
npm run node -- --config config0.json --genesis genesis0.json
```

Generate a transaction

```
npm run -s transaction -- --from <hexAddress> --to <hexAddress> --value 100 --number 0 --key <key.json> --payload "transaction info"
```

Generate a new key

```
npm run -s generateKey > ./keystore/alice.json
```

Generate a key from a private key

```
npm run -s generateKey -- --priv <hexKey> > ./keystore/alice.json
```

Notes

- Transactions are numbered. All transactions from a given sender must be increasingly numbered (otherwise the transaction will be refused).

## Config

Example:

```
{
  "minerAddress": "03d0127e341a3c7c53370f791913a217b2f5b257deeaa15e891d67b1e6615102ad",
  "node": {
    "port": 3000,
    "peers": [
      "ws://127.0.0.1:3000",
      "ws://127.0.0.1:3001",
      "ws://127.0.0.1:3002"
    ]
  },
  "restApi": {
    "port": 7000
  }
}
```

- `minerAddress` the public key to attribute mining rewards to.
- `node`: p2p node configuration
- `restApi`: http rest api configuration

## Genesis block

Example:

```
{
  "data": {
    "payload": "Genesis account provisionning",
    "transactions": [{
      "payload": "1000 -> alice",
      "number": 0,
      "to": "03d0127e341a3c7c53370f791913a217b2f5b257deeaa15e891d67b1e6615102ad",
      "value": 1000
    }, {
      "payload": "1000 -> bob",
      "number": 1,
      "to": "03847a470d39babe849ba10fd8dec2335d0c4bc7a26bacd3253f83a74b22d93994",
      "value": 1000
    }, {
      "payload": "1000 -> charlie",
      "number": 2,
      "to": "03fe803718c227abc3bb366d548a37bce44e4618e076fc0553507db1307d389c29",
      "value": 1000
    }]
  }
}
```

This is a valid block serialization. `from` address can be omitted and will be considered as the empty address. No other `from` addresses are allowed in genesis block. `payload` either at block level or transaction level are optional.

## Rest API

### `POST` `/transaction`

Creates a new transaction. Either:
- is rejected based on chain's current state and preceding pending transactions. In this case returns status 401 and the reason for rejection.
- is queued as pending transaction for mining. Returns an hex string representation of the transaction's hash.

Example

```
curl localhost:7001/transaction -X POST -H "Content-Type: application/json" -d '{"number":1,"from":"03d0127e341a3c7c53370f791913a217b2f5b257deeaa15e891d67b1e6615102ad","to":"03847a470d39babe849ba10fd8dec2335d0c4bc7a26bacd3253f83a74b22d93994","value":11,"payload":"","signature":"af7df849dac6f2f177a27a2a66da53e743fe00f5ede5d39f707f2279a57c047102c4a4a0bc95ecce973a12db1ec97bc9dffb927b5c4245bcb6610642a20ff057","hash":"41fffd782e5293b1e9d29e22bc5aef0d90c9f9218d7a6170b672a29b32d80e3b"}'
```

Returns

```
"41fffd782e5293b1e9d29e22bc5aef0d90c9f9218d7a6170b672a29b32d80e3b"
```

After transaction number 1 has been created, a transaction numbered 0 for the same sender will be refused:

```
curl localhost:7001/transaction -X POST -H "Content-Type: application/json" -d '{"number":0,"from":"03d0127e341a3c7c53370f791913a217b2f5b257deeaa15e891d67b1e6615102ad","to":"03847a470d39babe849ba10fd8dec2335d0c4bc7a26bacd3253f83a74b22d93994","value":1,"payload":"","signature":"ec41d6c10d127e1cb9dbb5430720177592285fe53e764799a76426fce1fa2ff13ba51f30f5411a3ff5015c2c0c066a8b3690fd7890db3b20f15367d6c4590840","hash":"4136e364ef2e38e24165956398da109c99ab153fa55cdaa32f94266bf554c7b5"}'
```

Returns

```
"Incoherent transaction ordering 0 for 03d0127e341a3c7c53370f791913a217b2f5b257deeaa15e891d67b1e6615102ad"
```

### `GET` `/blockCount`

Returns the current block count

Example

```
curl localhost:7001/blockCount
```

Returns

```
2
```

### `GET` `/block/:number`

Returns a JSON representation of block number `:number` (starting at 0).

Example

```
curl localhost:7001/block/1
```

Returns

```
{"number":1,"previous":"477119a0d8a42387261a94cf39fc859e1dd2e2211d7006bd403acc4822645823","data":{"payload":"","transactions":[{"number":1,"from":"03d0127e341a3c7c53370f791913a217b2f5b257deeaa15e891d67b1e6615102ad","to":"03847a470d39babe849ba10fd8dec2335d0c4bc7a26bacd3253f83a74b22d93994","value":11,"payload":"","signature":"af7df849dac6f2f177a27a2a66da53e743fe00f5ede5d39f707f2279a57c047102c4a4a0bc95ecce973a12db1ec97bc9dffb927b5c4245bcb6610642a20ff057","hash":"41fffd782e5293b1e9d29e22bc5aef0d90c9f9218d7a6170b672a29b32d80e3b"}]},"nonce":"061f80e5","minedBy":"03847a470d39babe849ba10fd8dec2335d0c4bc7a26bacd3253f83a74b22d93994","hash":"0000f4f5e7e30d4c4464878ae5916210473cc0987e866880a51d243f0d7f9a50"}
```

Genesis block

```
curl localhost:7001/block/0
```

Returns

```
{"number":0,"previous":"","data":{"payload":"Genesis account provisionning","transactions":[{"number":0,"from":"","to":"03d0127e341a3c7c53370f791913a217b2f5b257deeaa15e891d67b1e6615102ad","value":1000,"payload":"1000 -> alice","signature":"","hash":"43067570522afabdcb37b60ebe41434d72e4b5936b614f04c5b23a2332df8f4e"},{"number":1,"from":"","to":"03847a470d39babe849ba10fd8dec2335d0c4bc7a26bacd3253f83a74b22d93994","value":10000,"payload":"1000 -> bob","signature":"","hash":"b90522b3252ff0338e9c330d92bcae53a3a0ed7391948ea389c19f58e59d68bc"},{"number":2,"from":"","to":"03fe803718c227abc3bb366d548a37bce44e4618e076fc0553507db1307d389c29","value":1000,"payload":"1000 -> charlie","signature":"","hash":"eadbfae6fbd7ce04c82801552396475018079eb775dee4844c12063acefa9837"}]},"nonce":"","minedBy":"","hash":"477119a0d8a42387261a94cf39fc859e1dd2e2211d7006bd403acc4822645823"}
```

### `GET` `/transaction/:hash`

Returns a JSON representation of a transaction by its `:hash`

```
curl localhost:7001/transaction/41fffd782e5293b1e9d29e22bc5aef0d90c9f9218d7a6170b672a29b32d80e3b
```

Returns

```
{"number":1,"from":"03d0127e341a3c7c53370f791913a217b2f5b257deeaa15e891d67b1e6615102ad","to":"03847a470d39babe849ba10fd8dec2335d0c4bc7a26bacd3253f83a74b22d93994","value":11,"payload":"","signature":"af7df849dac6f2f177a27a2a66da53e743fe00f5ede5d39f707f2279a57c047102c4a4a0bc95ecce973a12db1ec97bc9dffb927b5c4245bcb6610642a20ff057","hash":"41fffd782e5293b1e9d29e22bc5aef0d90c9f9218d7a6170b672a29b32d80e3b"}
```

### `GET` `/balance/:address`

Returns the balance of the given `:address` in the current latest block.

```
curl localhost:7001/balance/03d0127e341a3c7c53370f791913a217b2f5b257deeaa15e891d67b1e6615102ad
```

Returns

```
989
```

### `GET` `/info/:address`

Returns the info about the given `:address` in the current latest block.

Example

```
curl localhost:7001/info/03d0127e341a3c7c53370f791913a217b2f5b257deeaa15e891d67b1e6615102ad
```

Returns

```
{"address":"03d0127e341a3c7c53370f791913a217b2f5b257deeaa15e891d67b1e6615102ad","balance":989,"lastTransactionNumber":1,"block":"0000f4f5e7e30d4c4464878ae5916210473cc0987e866880a51d243f0d7f9a50"}
```
