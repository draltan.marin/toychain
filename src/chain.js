import Chain from './lib/chain';
import config from './config';
import logger from './logger';

export const chain = new Chain(config.chain, logger);

chain.acceptBlock(config.genesis);

export default chain;
