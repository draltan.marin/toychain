import winston from 'winston';

const logger = winston.createLogger({
  silent: true
});

export default logger;
