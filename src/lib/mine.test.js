import Miner, {zeroes} from './mine';
import crypto from 'crypto';

process.on('unhandledRejection', (...params) => {
  params.forEach(param => {
    console.error(param);
  });
});

describe.skip('mining', () => {
  test('should compute hash of specified difficulty', async () => {
    let difficulty = 2;
    let miner = new Miner({difficulty});
    miner.setData('toto');

    let result = await miner.start();

    expect(zeroes(difficulty)(result.hash)).toBe(true);
  });

  test('should match resulting hash to data+nonce', async () => {
    let miner = new Miner({difficulty : 2});
    let data = 'should match resulting hash to data+nonce';
    miner.setData(data);
    let {nonce, hash} = await miner.start();
    let sha256 = crypto.createHash('sha256');
    sha256.update(data);
    sha256.update(nonce);
    expect(Buffer.compare(sha256.digest(), hash)).toBe(0);
  });

  test('should fail if no data set', async () => {
    let miner = new Miner({difficulty : 2});
    expect(miner.start()).rejects.toThrow('Use setData before start mining');
  });

  test('should be stoppable', () => {
    let miner = new Miner({difficulty : 4});
    miner.setData('stop please');
    let didCallStop = false;

    miner.start().then(() => {
      // May finish before miner is stopped
      // ignore.
    }).catch(error => {
      expect(error.message).stringContaining('Cancelled');
      expect(didCallStop).toBe(true);
    });

    setTimeout(() => {
      didCallStop = true;
      miner.stop();
    }, 1);
  });

  test.skip('should fail if already running', () => {
    let miner = new Miner({difficulty : 2});
    miner.setData('running');

    // TODO make sure the second call is invoked second
    // this creates an uncatched promise rejection
    expect(miner.start()).resolves.toBeDefined();
    expect(miner.start()).rejects.toThrow('Mining has already started');
  });

  test('should accept changing data while running', (done) => {
    let miner = new Miner({difficulty : 8});
    miner.setData('running');

    let dataChanged = false;

    miner.start().then(result => {
      expect(dataChanged).toBe(true);
      let {nonce, hash} = result;
      let sha256 = crypto.createHash('sha256');
      sha256.update('changed');
      sha256.update(nonce);
      expect(Buffer.compare(sha256.digest(), hash)).toBe(0);
      done();
    });

    setTimeout(() => {
      dataChanged = true;
      miner.setData('changed');
    }, 1);
  });
});
