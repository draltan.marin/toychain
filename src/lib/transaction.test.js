import Transaction from './transaction';
import {createKey} from './keys';

let from = createKey();
let to = createKey();

let trData = {
  number: 0,
  from: from.pub,
  to: to.pub,
  value: 1000,
};

test('should create a signed transaction if given a key', () => {
  let tr = new Transaction(trData, from.priv);

  expect(tr.verify()).toBe(true);
});

test('should create an unsigned transaction if not given a key', () => {
  let tr = new Transaction(trData);

  expect(tr.verify()).toBe(false);
});

test('should not verify a transaction if payload changed', () => {
  let tr = new Transaction(trData, from.priv);

  tr.setPayload('changed');
  expect(tr.verify()).toBe(false);
});

test('should not verify a transaction if value changed', () => {
  let tr = new Transaction(trData, from.priv);

  tr.value = 1001;
  expect(tr.verify()).toBe(false);
});

test('should not verify a transaction if from changed', () => {
  let tr = new Transaction(trData, from.priv);

  tr.from = to.pub;
  expect(tr.verify()).toBe(false);
});

test('should not verify a transaction if to changed', () => {
  let tr = new Transaction(trData, from.priv);

  tr.to = from.pub;
  expect(tr.verify()).toBe(false);
});

test('should change signData if to changed', () => {
  let tr = new Transaction(trData, from.priv);

  let signData1 = tr.getSignData();
  tr.to = from.pub;
  let signData2 = tr.getSignData();
  expect(Buffer.compare(signData1, signData2)).not.toBe(0);
});

test('should change signData if from changed', () => {
  let tr = new Transaction(trData, from.priv);

  let signData1 = tr.getSignData();
  tr.from = to.pub;
  let signData2 = tr.getSignData();
  expect(Buffer.compare(signData1, signData2)).not.toBe(0);
});

test('should change signData if value changed', () => {
  let tr = new Transaction(trData, from.priv);

  let signData1 = tr.getSignData();
  // console.log(signData1);
  tr.value = 1001;
  let signData2 = tr.getSignData();
  // console.log(signData2);
  expect(Buffer.compare(signData1, signData2)).not.toBe(0);
});

test('should change signData if payload changed', () => {
  let tr = new Transaction(trData, from.priv);

  let signData1 = tr.getSignData();
  tr.setPayload('toto');
  let signData2 = tr.getSignData();
  expect(Buffer.compare(signData1, signData2)).not.toBe(0);
});

test('should remove signature if payload changed', () => {
  let tr = new Transaction(trData, from.priv);
  expect(tr.isSigned()).toBe(true);
  tr.setPayload('toto');
  expect(tr.isSigned()).toBe(false);
});

test('should remove signature if from changed', () => {
  let tr = new Transaction(trData, from.priv);
  expect(tr.isSigned()).toBe(true);
  tr.setFrom(to.pub);
  expect(tr.isSigned()).toBe(false);
});

test('should remove signature if to changed', () => {
  let tr = new Transaction(trData, from.priv);
  expect(tr.isSigned()).toBe(true);
  tr.setTo(from.pub);
  expect(tr.isSigned()).toBe(false);
});

test('should remove signature if value changed', () => {
  let tr = new Transaction(trData, from.priv);
  expect(tr.isSigned()).toBe(true);
  tr.setValue(1001);
  expect(tr.isSigned()).toBe(false);
});

test('should remove signature if transaction number changed', () => {
  let tr = new Transaction(trData, from.priv);
  expect(tr.isSigned()).toBe(true);
  tr.setTransactionNumber(2);
  expect(tr.isSigned()).toBe(false);
});

test('should verify a transaction if signed', () => {
  let tr = new Transaction(trData);
  tr.sign(from.priv);
  expect(tr.verify()).toBe(true);
});

test('should verify a modified transaction if explicitly signed after a change', () => {
  let tr = new Transaction(trData, from.priv);

  tr.setPayload('changed');
  tr.sign(from.priv);
  expect(tr.verify()).toBe(true);
});

test('should refuse to sign with an unmatched key', () => {
  let tr = new Transaction(trData);

  expect(() => tr.sign(to.priv)).toThrow();
  expect(tr.verify()).toBe(false);
});


test('should preserve data for hash calculation after encode and decode', () => {
  let tr = new Transaction(trData);

  let encodings = ['base64', 'hex'];
  encodings.forEach(encoding => {
    let mining1 = tr.getMiningData();
    let mining2 = Transaction.fromObject(tr.toObject(encoding), encoding).getMiningData();
    expect(Buffer.compare(mining1, mining2)).toBe(0);
  });
});

test('should preserve signature after encode and decode', () => {
  let tr = new Transaction(trData);

  let encodings = ['base64', 'hex'];
  encodings.forEach(encoding => {
    let tr1 = Transaction.fromObject(tr.toObject(encoding), encoding);
    expect(tr1.verify()).toBe(false);
  });

  tr.sign(from.priv);
  encodings.forEach(encoding => {
    let tr1 = Transaction.fromObject(tr.toObject(encoding), encoding);
    expect(tr1.verify()).toBe(true);
    expect(Buffer.compare(tr1.hash, tr.hash)).toBe(0);
  });
});
