import Block from './block';
import {zeroes} from './mine';
import {verifyPublic} from './keys';
import defaultLogger from './voidLogger';
/*
  A chain verifies blocks constraints and can compare to
  other chains.
*/
// TODO compare chains
class Chain {
  constructor(
    options = {},
    logger = defaultLogger
  ) {
    let {
      difficulty,
      decimals = 0,
      minerReward = 100,
      minerAddress = '',
    } = options;
    this.logger = logger;
    // chain state (all keys are hex hashes string values)
    this.genesis = Buffer.alloc(0); // genesis hash
    this.current = Buffer.alloc(0); // block hash
    this.blockIndex = { // block hash => {block, balance, lastTransactionNumber}
      '': {
        balance: {},
        lastTransactionNumber: {},
      }
    };
    this.transactionIndex = {}; // transaction hash => block hash
    this.pendingBlocks = {}; // previous hash => block

    // chain config
    this.hashValidation = zeroes(difficulty);
    this.decimals = decimals;
    this.minerReward = minerReward;
    this.minerAddress = Buffer.from(minerAddress);
    if (this.minerAddress.length > 0 && !verifyPublic(this.minerAddress)) {
      throw new Error('invalid minedby address');
    }
  }

  createBlock(blockData) {
    return new Block(blockData, this.currentBlock(), this.minerAddress);
  }

  /*
    returns an object
    {
      accepted: a list of accepted block's hashes (including possibly previously pending blocks)
      rejected: a list of rejected block's hashes
    }
    or
    {
      pending: a list of pending block's hashes
    }
  */
  acceptBlock(newBlock) {
    // make sure methods are those of Block
    newBlock = Block.fromObject(newBlock.toObject('hex'), 'hex');
    let acceptedBlock = this.getBlockByHash(newBlock.hash);

    if (acceptedBlock !== undefined){
      if (newBlock.equals(acceptedBlock)) {
        // if existing expect no other previous block acceptances
        return {
          accepted: [newBlock.hash],
        };
      } else {
        return {
          rejected: [newBlock.hash],
        };
      }
    }

    let previousState = this.getState(newBlock.previous);
    let previousBlock = this.getBlockByHash(newBlock.previous);

    if (!newBlock.isGenesis() && previousBlock === undefined) {
      // couldn't be found
      let previousKey = newBlock.previous.toString('hex');
      this.pendingBlocks[previousKey] = this.pendingBlocks[previousKey] || [];
      // is new block already pending ?
      let isPending = false;
      this.pendingBlocks[previousKey].forEach(block => {
        if (block.equals(newBlock)) {
          isPending = true;
        }
      });
      if (!isPending) {
        this.pendingBlocks[previousKey].push(newBlock);
      }
      return {
        pending: [newBlock.hash],
      };
    } else {
      try {
        let result = this.validateBlock(newBlock, previousBlock, previousState);
        if (newBlock.isGenesis()) {
          if (this.genesis.length === 0) {
            this.genesis = newBlock.hash;
          } else {
            return {
              rejected: [newBlock.hash]
            };
          }
        }

        this.logger.info(`Valid block: ${newBlock.hash.toString('hex')}`);
        // update indexes
        this.blockIndex[newBlock.hash.toString('hex')] = result;

        newBlock.data.transactions.forEach(transaction => {
          // TODO handle collision
          this.transactionIndex[transaction.hash.toString('hex')] = transaction;
        });

        if (
          this.current.length === 0 || // not yet initialized
          newBlock.number > this.currentBlock().number // chain at this point is greater than current
        ) { // note: genesis block can't be changed
          this.current = newBlock.hash;
          this.logger.info(`Current block: ${this.current.toString('hex')}`);
        }

        let next = this.pendingBlocks[newBlock.hash.toString('hex')];
        delete this.pendingBlocks[newBlock.hash.toString('hex')];

        if (next !== undefined) {
          let acceptedResults = [newBlock.hash], rejectedResults = [];
          next.forEach(block => {
            let {accepted = [], rejected = []} = this.acceptBlock(block);
            acceptedResults = [...acceptedResults, ...accepted];
            rejectedResults = [...rejectedResults, ...rejected];
          });

          return { // return all accepted and rejected blocks
            accepted: acceptedResults,
            rejected: rejectedResults,
          };
        } else {
          return { // no errors or pending expected
            accepted: [newBlock.hash],
          };
        }

      } catch (err) {
        return {
          rejected: [newBlock.hash],
        };
      }
    }
  }

  validateBlock(block, previousBlock, previousState) {
    let expectedBlockNumber = previousBlock === undefined ? 0: previousBlock.number + 1;

    if (block.number !== expectedBlockNumber) {
      throw new Error('Invalid block number');
    }

    // check previous hash
    if (expectedBlockNumber === 0) { // should be genesis block
      if (!block.isGenesis()) {
        throw new Error('Invalid genesis block');
      }
    } else if (
      Buffer.compare(
        block.previous,
        previousBlock.hash
      ) !== 0
    ) {
      throw new Error('Previous hash mismatch');
    }

    // check hash difficulty
    if (!block.isGenesis() && !this.hashValidation(block.hash)) {
      throw new Error('Invalid hash difficulty');
    }

    // check block hash is correct, should never happen
    if (
      Buffer.compare(
        block.hash,
        block.computeHash()
      ) !== 0
    ) {
      throw new Error('Incorrect block hash computation');
    }

    // TODO move elsewere
    // no previous block with same hash
    if (this.getBlockByHash(block.hash) !== undefined) {
      throw new Error('Duplicate block hash');
    }

    let data = block.data;
    if (data.transactions.length <= 0) {
      throw new Error('Empty transaction data');
    }

    let {balance, lastTransactionNumber} = previousState;

    data.transactions.forEach(transaction => {
      this.validateTransaction(
        transaction,
        block.isGenesis(),
        balance,
        lastTransactionNumber,
        (err, result) => {
          if (err) throw err;
          balance = result.balance;
          lastTransactionNumber = result.lastTransactionNumber;
        }
      );
    });

    // Miner reward
    if (!block.isGenesis()) {
      if (block.hasValidMiner()) {
        let minerkey = block.minedBy.toString('hex');
        balance[minerkey] = (balance[minerkey] || 0) + this.minerReward * Math.pow(10,this.decimals);
      }
    }

    return { // resulting state
      block,
      balance,
      lastTransactionNumber,
    };
  }

  validateTransaction(
    transaction,
    isGenesis,
    _balance,
    _lastTransactionNumber,
    done
  ) {
    let balance = {... _balance};
    let lastTransactionNumber = {... _lastTransactionNumber};

    let {number, from, to, value} = transaction;

    if (
      (isGenesis && from.length !== 0) || // expect genesis transactions from ''
      (!isGenesis && !verifyPublic(from))) {
      return done(new Error('Unexpected transaction from'));
    }

    if (to.length > 0 && !verifyPublic(to)) { // allow sending to 0 address
      return done(new Error('Invalid to address'));
    }

    if (!isGenesis && !transaction.verify()) {
      return done(new Error('Invalid transaction signature'));
    }

    if (value <= 0) {
      return done(new Error('Invalid non-positive transaction value'));
    }

    let toKey = to.toString('hex');
    let fromKey = from.toString('hex');

    // check transaction ordering
    let lastNumber = lastTransactionNumber[fromKey];
    if (lastNumber !== undefined && number <= lastNumber) {
      return done(new Error(`Incoherent transaction ordering ${number} for ${fromKey}`));
    }
    lastTransactionNumber[fromKey] = number;

    // update balance
    balance[toKey] = (balance[toKey] || 0) + value;

    if (!isGenesis) {
      balance[fromKey] = (balance[fromKey] || 0) - value;
      if (balance[fromKey] < 0) {
        return done(new Error('Insufficient balance'));
      }
    }

    return done(null, {
      balance,
      lastTransactionNumber,
    });
  }

  length() {
    if (this.current.length === 0) { // no current block yet
      return 0;
    }
    return this.currentBlock().number + 1;
  }

  id() {
    return this.genesis;
  }

  currentBlock() {
    return this.blockIndex[this.current.toString('hex')].block;
  }

  currentBlockNumber() {
    return this.currentBlock().number;
  }

  getTransactionByHash(hash) {
    return this.transactionIndex[hash.toString('hex')];
  }

  hasTransaction(transaction, fromBlockHash) {
    let hash = fromBlockHash || this.current;
    let block = this.getBlockByHash(hash);
    for (let i = 0; i < block.data.transactions.length; i ++){
      let tr = block.data.transactions[i];
      if (Buffer.compare(tr.hash, transaction.hash) === 0) {
        return true;
      }
    }
    if (block.previous.length === 0) {
      return false;
    }
    return this.hasTransaction(transaction, block.previous);
  }

  getBlockByHash(hash) {
    return this.getState(hash).block;
  }

  getBlockByNumber(blockNumber) {
    let block = this.currentBlock();
    if (blockNumber < 0 || blockNumber > block.number) {
      return undefined;
    }
    while (block.number != blockNumber) {
      block = this.getBlockByHash(block.previous);
    }
    return block;
  }

  getState(hash) {
    let key = (hash || this.current).toString('hex');
    return this.blockIndex[key] || {};
  }

  getBalance(address, hash) {
    let { balance = {} } = (this.getState(hash) || {});
    return balance[address.toString('hex')] || 0;
  }

  getInfo(address, hash) {
    let state = this.getState(hash);
    if (!state) {
      return {};
    }
    let {
      balance = {},
      lastTransactionNumber = {},
      block,
    } = state;
    return {
      address: address.toString('hex'),
      balance: balance[address.toString('hex')] || 0,
      lastTransactionNumber: lastTransactionNumber[address.toString('hex')],
      block: block ? block.hash.toString('hex') : undefined,
    };
  }
}

export default Chain;

const is = (status) => (result, block) => {
  if (!result[status] || result[status].length === 0) {
    return false;
  }
  let found = false;
  result[status].forEach(hash => {
    if (Buffer.compare(hash, block.hash) === 0) {
      found = true;
    }
  });
  return found;
};

export const isAccepted = is('accepted');
export const isRejected = is('rejected');
export const isPending = is('pending');
