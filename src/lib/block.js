import crypto from 'crypto';
import {numToBuffer} from './util';
import BlockData from './blockData';
import {verifyPublic} from './keys';

class Block {
  constructor(data, previousBlock, minedBy = '') {
    if (previousBlock !== undefined) {
      this.number = previousBlock.number + 1;
      this.previous = previousBlock.hash;
    } else {
      this.number = 0;
      this.previous = Buffer.alloc(0);
    }
    this.data = data;
    this.nonce = Buffer.alloc(0);
    this.setMiner(minedBy);
  }

  isGenesis() {
    return this.number === 0 && this.previous.length === 0;
  }

  setNonce(nonce) {
    this.nonce = Buffer.from(nonce);
    this.hash = this.computeHash();
  }

  setData(data) {
    this.data = data;
    this.hash = this.computeHash();
  }

  setMiner(minedBy = '') {
    if (this.isGenesis()) { // don't add miner
      this.minedBy = Buffer.alloc(0);
    } else {
      let address = Buffer.from(minedBy);
      if (address.length > 0 && !verifyPublic(address)) {
        throw new Error('invalid minedby address');
      }
      this.minedBy = address;
    }
    this.hash = this.computeHash();
  }

  hasValidMiner() {
    return this.minedBy.length > 0 && verifyPublic(this.minedBy);
  }

  computeHash() {
    let sha256 = crypto.createHash('sha256');
    sha256.update(this.getMiningData());
    sha256.update(this.nonce);
    return sha256.digest();
  }

  getMiningData() {
    return Buffer.concat([
      numToBuffer(this.number),
      this.previous,
      this.data.getMiningData(),
      this.minedBy,
    ]);
  }

  equals(block) {
    return Buffer.compare(
      this.computeHash(),
      block.computeHash()
    ) === 0;
  }

  static fromObject({
    number = 0,
    previous = '',
    data,
    nonce = '',
    minedBy = '',
  }, encoding = 'hex') {
    let block = new Block(BlockData.fromObject(data, encoding));
    block.number = number;
    block.previous = Buffer.from(previous, encoding);
    block.nonce = Buffer.from(nonce, encoding);
    block.minedBy = Buffer.from(minedBy, encoding);
    block.hash = block.computeHash();
    return block;
  }

  toObject(encoding = 'hex') {
    return {
      number: this.number,
      previous: this.previous.toString(encoding),
      data: this.data.toObject(encoding),
      nonce: this.nonce.toString(encoding),
      minedBy: this.minedBy.toString(encoding),
      hash: this.hash.toString(encoding),
    };
  }

  toString(encoding = 'hex'){
    return JSON.stringify(this.toObject(encoding));
  }
}

export default Block;
