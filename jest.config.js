let ignore = [
  '/node_modules/',
  '/distribution/',
];

module.exports = {
  verbose: true,
  coveragePathIgnorePatterns: ignore,
  testPathIgnorePatterns: ignore,
  watchPathIgnorePatterns: ['/distribution/'],
};
