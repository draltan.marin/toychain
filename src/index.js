import config from './config';
import app from './app';
import {node} from './p2pHandlers';

app.listen(config.restApi.port);
node.start(config.node);
