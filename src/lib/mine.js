import crypto from 'crypto';

class Miner {
  /*
  Options:
  - hash validation:
    Miner accepts either a validator function or a difficulty level
    (validator function takes precedence)
    If a validator is set, it is of the form:
    (hash) => { return true; }
    If a difficulty level n is set, it will check that the first
    n bytes of the resulting hash are zeroes.
    (default 0 bytes difficulty)
  - nonceLenght: if set, it will generate nonces of nonceLenght bytes.
    (default 4 bytes)
  */
  constructor (options = {}){
    let {
      difficulty,
      nonceLenght = 4,
      validator
    } = options;
    this.isValid = validator || zeroes(difficulty);
    this.shouldStop = false;
    this.nonceLenght = nonceLenght;
    this.chunkSize = 10000;
    this.data = null;
    this.isRunning = false;
  }

  /*
  Use before start mining
  To be used when adding new transactions to a mining block
  */
  setData (data) {
    this.data = Buffer.from(data);
  }

  stop () {
    if (this.isRunning) {
      this.shouldStop = true;
    }
  }

  /*
  Should have setData before mining
  */
  start () {
    if (this.data === null) {
      return Promise.reject(new Error('Use setData before start mining'));
    }
    if (this.isRunning) {
      return Promise.reject(new Error('Mining has already started'));
    }
    this.isRunning = true;
    this.shouldStop = false;
    return new Promise((resolve, reject) => {
      let totalIterations = 0;

      // Mining by chunks allows other callbacks
      // to be executed in between. Otherwise,
      // once the mining loop is started, there would
      // no be place to other code to be executed (neither
      // callbacks nor timers).
      let miningChunk = () => {
        // Check if mining should stop
        if (this.shouldStop) {
          this.isRunning = false;
          reject(new Error(`Mining: Cancelled after ${totalIterations} iterations.`));
        }

        // Main mining loop
        let i = 0;
        let nonce = null;
        let hash = null;
        let found = false;
        for (i = 0; i < this.chunkSize; i++) {
          nonce = Buffer.allocUnsafe(this.nonceLenght);
          crypto.randomFillSync(nonce);

          let sha256 = crypto.createHash('sha256');
          sha256.update(this.data);
          sha256.update(nonce);
          hash = sha256.digest();
          if ((found = this.isValid(hash))){
            break;
          }
        }
        totalIterations += i;

        // Check result
        if (found) {
          this.isRunning = false;
          resolve({
            nonce,
            hash,
          });
        } else { // Otherwise set a new mining chunk in the execution loop
          setImmediate(miningChunk);
        }
      };

      // Start mining
      setImmediate(miningChunk);
    });
  }
}

export default Miner;

export const zeroes = (difficulty = 0) => (hash) => {
  let valid = true;
  for(let i = 0; i < difficulty; i++){
    let byte = hash.readInt8(i);
    if (byte !== 0) {
      valid = false;
      break;
    }
  }
  return valid;
};
