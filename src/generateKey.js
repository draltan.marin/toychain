import {createKey, format} from './lib/keys';
import minimist from 'minimist';

// read config file as specified in cli options
let args = minimist(process.argv.slice(2)); // ignore node and filename
let {priv = ''} = args;
priv = Buffer.from(priv, 'hex');

process.stdout.write(`${format(createKey(priv))}\n`);
