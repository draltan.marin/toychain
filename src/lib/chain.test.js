import Chain, {isAccepted, isRejected, isPending} from './chain';
import Block from './block';
import BlockData from './blockData';
import Transaction from './transaction';
import Miner, {zeroes} from './mine';
import {createKey} from './keys';

// process.on('unhandledRejection', (...params) => {
//   params.forEach(param => {
//     console.error(param);
//   });
// });

let alice = createKey();
let bob = createKey();
let charlie = createKey();

let genesisData = new BlockData();
let initialBalance = 1000;

genesisData.addTransaction(new Transaction({
  number: 0,
  from: '',
  to: alice.pub,
  value: initialBalance,
}));
genesisData.addTransaction(new Transaction({
  number: 1,
  from: '',
  to: alice.pub,
  value: initialBalance,
}));
genesisData.addTransaction(new Transaction({
  number: 2,
  from: '',
  to: charlie.pub,
  value: initialBalance,
}));

test('should start by a genesis block', () => {
  let chain = new Chain();
  let block = chain.createBlock(new BlockData());
  expect(block.isGenesis()).toBe(true);
});

test('should create blocks with proper minedby', () => {
  let chain = new Chain({minerAddress: alice.pub});
  let genesis = chain.createBlock(genesisData);
  // no miner for genesis block
  expect(genesis.minedBy).toHaveLength(0);
  chain.acceptBlock(genesis);

  let block = chain.createBlock(new BlockData());
  // following blocks should be mined by chain's minerAddress
  expect(Buffer.compare(block.minedBy, alice.pub)).toBe(0);
});

test('should accept unsigned transactions in genesis block', () => {
  let chain = new Chain();
  genesisData.transactions.forEach(transaction => {
    expect(transaction.isSigned()).toBe(false);
  });

  let genesis = chain.createBlock(genesisData);
  let result = chain.acceptBlock(genesis);
  expect(result.accepted).toBeDefined();
  expect(isAccepted(result, genesis)).toBe(true);
});

test('should accept non genesis block', () => {
  let chain = new Chain();
  let genesis = chain.createBlock(genesisData);
  chain.acceptBlock(genesis);

  let data0 = new BlockData();
  data0.addTransaction(new Transaction({
    number: 0,
    from: alice.pub,
    to: bob.pub,
    value: initialBalance,
  }, alice.priv)); // create a signed transaction

  let block0 = chain.createBlock(data0);
  let result = chain.acceptBlock(block0);

  expect(isAccepted(result, block0)).toBe(true);
});

test('should refuse wrong miner address', () => {
  expect(() => new Chain({minerAddress: 'alice'})).toThrow();
});

test('should refuse insufficient balance transactions', () => {
  let chain = new Chain();
  let genesis = chain.createBlock(genesisData);
  chain.acceptBlock(genesis);

  let balance = chain.getBalance(alice.pub);
  let data0 = new BlockData();

  // values sum balance + 1 > balance

  data0.addTransaction(new Transaction({
    number: 0,
    from: alice.pub,
    to: bob.pub,
    value: balance - 1,
  }, alice.priv)); // create a signed transaction

  data0.addTransaction(new Transaction({
    number: 1,
    from: alice.pub,
    to: bob.pub,
    value: 2,
  }, alice.priv)); // create a signed transaction

  let block0 = chain.createBlock(data0);
  let result = chain.acceptBlock(block0);
  expect(isAccepted(result, block0)).toBe(false);
  expect(isRejected(result, block0)).toBe(true);
});

test('should refuse invalid singature transactions', () => {
  let chain = new Chain();
  let genesis = chain.createBlock(genesisData);
  chain.acceptBlock(genesis);

  let data0 = new BlockData();
  let tr = new Transaction({
    number: 0,
    from: alice.pub,
    to: bob.pub,
    value: initialBalance,
  }, alice.priv); // create a signed transaction

  // this makes the signature invalid
  tr.value = initialBalance - 1;
  expect(tr.verify()).toBe(false);

  data0.addTransaction(tr);

  let block0 = chain.createBlock(data0);
  let result = chain.acceptBlock(block0);

  expect(isRejected(result, block0)).toBe(true);
});

test('should refuse invalid from transactions', () => {
  let chain = new Chain();
  let genesis = chain.createBlock(genesisData);
  chain.acceptBlock(genesis);

  let data0 = new BlockData();

  data0.addTransaction(new Transaction({
    number: 0,
    from: 'alice',
    to: bob.pub,
    value: 1,
  }));

  let block0 = chain.createBlock(data0);
  let result = chain.acceptBlock(block0);
  expect(isAccepted(result, block0)).toBe(false);
  expect(isRejected(result, block0)).toBe(true);
});

test('should refuse invalid to transactions', () => {
  let chain = new Chain();
  let genesis = chain.createBlock(genesisData);
  chain.acceptBlock(genesis);

  let data0 = new BlockData();

  data0.addTransaction(new Transaction({
    number: 0,
    from: alice.pub,
    to: 'bob',
    value: 1,
  }), alice.priv);

  let block0 = chain.createBlock(data0);
  let result = chain.acceptBlock(block0);
  expect(isAccepted(result, block0)).toBe(false);
  expect(isRejected(result, block0)).toBe(true);
});

test('should refuse invalid numbered transactions', () => {
  let chain = new Chain();
  let genesis = chain.createBlock(genesisData);
  chain.acceptBlock(genesis);

  let data0 = new BlockData();
  data0.addTransaction(new Transaction({
    number: 1,
    from: alice.pub,
    to: bob.pub,
    value: 1,
  }, alice.priv));

  let block0 = chain.createBlock(data0);
  // console.log(block0.hash);
  let result0 = chain.acceptBlock(block0);
  expect(isAccepted(result0, block0)).toBe(true);

  let accountInfo = chain.getInfo(alice.pub);
  expect(accountInfo.lastTransactionNumber).toBe(1);

  let data1 = new BlockData();
  data1.addTransaction(new Transaction({
    number: 0,
    from: alice.pub,
    to: 'bob',
    value: 1,
  }), alice.priv);

  let block1 = chain.createBlock(data1);
  let result = chain.acceptBlock(block1);
  expect(isAccepted(result, block1)).toBe(false);
  expect(isRejected(result, block1)).toBe(true);
});

test('should set pending on missing previous block', () => {
  let chain = new Chain();
  let genesis = chain.createBlock(genesisData);
  chain.acceptBlock(genesis);

  let data1 = new BlockData();
  data1.addTransaction(new Transaction({
    number: 0,
    from: alice.pub,
    to: bob.pub,
    value: 10,
  }, alice.priv)); // create a signed transaction

  let data2 = new BlockData();
  data2.addTransaction(new Transaction({
    number: 1,
    from: alice.pub,
    to: bob.pub,
    value: 10,
  }, alice.priv)); // create a signed transaction

  // concurrent longer chain
  let block1 = new Block(data1, genesis);
  let block2 = new Block(data2, block1);

  let result = chain.acceptBlock(block2);
  expect(isPending(result, block2)).toBe(true);
  expect(Buffer.compare(chain.current, genesis.hash)).toBe(0);

  // should change to block2 once missing block is accepted
  chain.acceptBlock(block1);
  expect(Buffer.compare(chain.current, block2.hash)).toBe(0);
});

test('should ignore wrong block hash computation', () => {
  let chain = new Chain();
  let genesis = chain.createBlock(genesisData);
  chain.acceptBlock(genesis);

  let data0 = new BlockData();
  data0.addTransaction(new Transaction({
    number: 1,
    from: alice.pub,
    to: bob.pub,
    value: 1,
  }, alice.priv));

  let block0 = chain.createBlock(data0);
  block0.nonce = Buffer.from('12345678', 'hex');
  expect(Buffer.compare(block0.hash, block0.computeHash())).not.toBe(0);
  let result = chain.acceptBlock(block0);
  expect(isAccepted(result, block0)).toBe(false);
  block0.hash = block0.computeHash();
  expect(isAccepted(result, block0)).toBe(true);
});

test('should refuse wrong block hash difficulty', (done) => {
  let difficulty = 2;
  let chain = new Chain({difficulty});
  let genesis = chain.createBlock(genesisData);
  chain.acceptBlock(genesis);

  let data0 = new BlockData();
  data0.addTransaction(new Transaction({
    number: 0,
    from: alice.pub,
    to: bob.pub,
    value: initialBalance,
  }, alice.priv)); // create a signed transaction

  let block0 = chain.createBlock(data0);
  let miner = new Miner({difficulty: difficulty - 1});

  miner.setData(block0.getMiningData());
  miner.start()
    .then(({nonce}) => {
      block0.setNonce(nonce);
      expect(zeroes(difficulty - 1)(block0.hash)).toBe(true);
      expect(zeroes(difficulty)(block0.hash)).toBe(false);

      let result = chain.acceptBlock(block0);

      expect(isAccepted(result, block0)).toBe(false);
      expect(isRejected(result, block0)).toBe(true);
      done();
    })
    .catch();
});

test('should refuse empty transaction block', () => {
  let chain = new Chain();
  let block = chain.createBlock(new BlockData());
  let result = chain.acceptBlock(block);

  expect(isRejected(result, block)).toBe(true);
});

test('should update balance', () => {
  let chain = new Chain();
  let genesis = chain.createBlock(genesisData);
  chain.acceptBlock(genesis);
  let value = 42;
  let data0 = new BlockData();
  data0.addTransaction(new Transaction({
    number: 0,
    from: alice.pub,
    to: bob.pub,
    value,
  }, alice.priv)); // create a signed transaction

  let block = chain.createBlock(data0);
  let initialBalance = {
    alice: chain.getBalance(alice.pub),
    bob: chain.getBalance(bob.pub),
  };
  chain.acceptBlock(block);
  let finalBalance = {
    alice: chain.getBalance(alice.pub),
    bob: chain.getBalance(bob.pub),
  };
  expect(initialBalance.alice - value).toBe(finalBalance.alice);
  expect(initialBalance.bob + value).toBe(finalBalance.bob);
});

test('should update transaction numbering', () => {
  let chain = new Chain();
  let genesis = chain.createBlock(genesisData);
  chain.acceptBlock(genesis);

  let data0 = new BlockData();
  let transactionNumber = 42;
  data0.addTransaction(new Transaction({
    number: transactionNumber,
    from: alice.pub,
    to: bob.pub,
    value: 10,
  }, alice.priv)); // create a signed transaction

  let block = chain.createBlock(data0);
  chain.acceptBlock(block);
  let accountInfo = chain.getInfo(alice.pub, block.hash);
  expect(accountInfo.lastTransactionNumber).toBe(transactionNumber);
});

test('should attribute mining reward', () => {
  let minerReward = 50;
  let chain = new Chain({
    minerReward,
    minerAddress: alice.pub
  });
  let genesis = chain.createBlock(genesisData);
  chain.acceptBlock(genesis);

  let data0 = new BlockData();
  data0.addTransaction(new Transaction({
    number: 0,
    from: charlie.pub,
    to: bob.pub,
    value: 10,
  }, charlie.priv)); // create a signed transaction

  let block = chain.createBlock(data0);

  let initialBalance = chain.getBalance(alice.pub);
  chain.acceptBlock(block);
  let finalBalance = chain.getBalance(alice.pub);
  expect(initialBalance + minerReward).toBe(finalBalance);
});

test('should switch to longer chain', () => {
  let chain = new Chain();
  let genesis = chain.createBlock(genesisData);
  chain.acceptBlock(genesis);

  let data0 = new BlockData();
  data0.addTransaction(new Transaction({
    number: 0,
    from: alice.pub,
    to: bob.pub,
    value: 10,
  }, alice.priv)); // create a signed transaction

  let data1 = new BlockData();
  data1.addTransaction(new Transaction({
    number: 0,
    from: alice.pub,
    to: bob.pub,
    value: 10,
  }, alice.priv)); // create a signed transaction

  let data2 = new BlockData();
  data2.addTransaction(new Transaction({
    number: 1,
    from: alice.pub,
    to: bob.pub,
    value: 10,
  }, alice.priv)); // create a signed transaction

  // smaller chain
  let block0 = new Block(data0, genesis);
  // concurrent longer chain
  let block1 = new Block(data1, genesis);
  let block2 = new Block(data2, block1);

  chain.acceptBlock(block0);
  expect(Buffer.compare(chain.current, block0.hash)).toBe(0);

  chain.acceptBlock(block1);
  // should still be block0 as block1 is not longer
  expect(Buffer.compare(chain.current, block0.hash)).toBe(0);

  chain.acceptBlock(block2);
  // should have changed to block2
  expect(Buffer.compare(chain.current, block2.hash)).toBe(0);
});
