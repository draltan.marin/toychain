import Transaction from './transaction';
import {utf8} from './util';

class BlockData {
  constructor() {
    this.transactions = [];
    this.payload = Buffer.alloc(0);
    this.index = {};
  }

  setPayload(payload = '') {
    this.payload = Buffer.from(payload);
  }

  addTransaction(transaction) {
    let hash = transaction.computeHash();
    if (this.hasTransaction(hash)){
      throw new Error('Attempting to add a duplicate transaction');
    }
    this.index[hash.toString('hex')] = this.transactions.length;
    this.transactions.push(transaction);
  }

  getMiningData() {
    return Buffer.concat([
      this.payload,
      ...(this.transactions.map(transaction => transaction.getMiningData()))
    ]);
  }

  getTransaction(hash) {
    let trIndex = this.transactionIndex(hash);
    if (trIndex !== undefined) {
      return this.transactions[trIndex];
    }
  }

  transactionIndex(hash) {
    return this.index[hash.toString('hex')];
  }

  hasTransaction(hash) {
    return this.transactionIndex(hash) !== undefined;
  }

  static fromObject({
    transactions,
    payload = '',
  }, encoding = 'hex') {
    let data = new BlockData();
    data.setPayload(utf8(payload));
    transactions.forEach(transaction => {
      data.addTransaction(Transaction.fromObject(transaction, encoding));
    });
    return data;
  }

  toObject(encoding = 'hex') {
    return {
      payload: this.payload.toString('utf8'),
      transactions: this.transactions.map(transaction => transaction.toObject(encoding)),
    };
  }

  toString(encoding = 'hex') {
    return JSON.stringify(this.toObject(encoding));
  }
}

export default BlockData;
