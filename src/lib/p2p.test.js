import Node from './p2p';

// process.on('unhandledRejection', (...params) => {
//   params.forEach(param => {
//     console.error(param);
//   });
// })

test('should create all sockets', async () => {
  let messageHandler = () => {
    // do nothing
  };

  let peers = [
    'ws://127.0.0.1:4000',
    'ws://127.0.0.1:4001',
    'ws://127.0.0.1:4002',
  ];

  let node1 = new Node(messageHandler);
  let node2 = new Node(messageHandler);
  let node3 = new Node(messageHandler);

  node1.start({port: 4000, peers});
  node2.start({port: 4001, peers});
  node3.start({port: 4002, peers});

  setTimeout(() => {
    expect(Object.keys(node3.connections)).toHaveLength(2);
  }, 2000);
});

test('should broadcast to all nodes', async () => {
  let received = {};
  let messageHandler = (data, from) => {
    received[from] = (received[from] || 0) + 1;
  };

  let peers = [
    'ws://127.0.0.1:5000',
    'ws://127.0.0.1:5001',
    'ws://127.0.0.1:5002',
  ];

  let node1 = new Node(messageHandler);
  let node2 = new Node(messageHandler);
  let node3 = new Node(messageHandler);

  node1.start({port: 5000, peers});
  node2.start({port: 5001, peers});
  node3.start({port: 5002, peers});

  setTimeout(() => {
    node1.broadCast('message');
  }, 1000);

  setTimeout(() => {
    peers.forEach(peer => {
      expect(received[peer]).toBe(2);
    });
  }, 2000);
});
