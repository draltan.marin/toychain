import crypto from 'crypto';
import secp256k1 from 'secp256k1';

export const createKey = (privateKey = '') => {
  let priv = Buffer.from(privateKey);
  while (!secp256k1.privateKeyVerify(priv)){
    priv = crypto.randomBytes(32);
  }
  let pub = secp256k1.publicKeyCreate(priv);
  return {
    priv,
    pub,
  };
};

export const sign = (data, priv) => (secp256k1.sign(data, priv).signature);

export const verify = (data, signature, pub) => secp256k1.verify(
  data,
  signature,
  pub
);

export const getPublic = (priv) => secp256k1.publicKeyCreate(priv);

export const verifyPublic = (pub) => secp256k1.publicKeyVerify(pub);

export const format = ({priv, pub}, encoding = 'hex') => {
  return JSON.stringify(
    {
      priv: priv.toString(encoding),
      pub: pub.toString(encoding),
    },
    null,
    2
  );
};
