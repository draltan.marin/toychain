import express from 'express';
import bodyParser from 'body-parser';
import logger from 'morgan';
import Transaction from './lib/transaction';

import {addTransaction} from './minerState';
import chain from './chain';
import {node} from './p2pHandlers';

let app = express();
app.use(logger('dev'));
app.use(bodyParser.json());

app.get('/blockCount', (req, res) => {
  res.json(chain.length());
});

app.get('/block/:number', (req, res) => {
  let number = parseInt(req.params.number); // NaN if not a number
  let block = chain.getBlockByNumber(number);
  if (block) {
    res.json(block.toObject('hex'));
  } else {
    res.sendStatus(404);
  }
});

app.get('/transaction/:hash', (req, res) => {
  let hash = Buffer.from(req.params.hash, 'hex');
  let transaction = chain.getTransactionByHash(hash);
  if (transaction) {
    res.json(transaction.toObject('hex'));
  } else {
    res.sendStatus(404);
  }
});

app.get('/balance/:address', (req, res) => {
  let address = Buffer.from(req.params.address, 'hex');
  let balance = chain.getBalance(address);
  res.json(balance);
});

app.get('/info/:address', (req, res) => {
  let address = Buffer.from(req.params.address, 'hex');
  let info = chain.getInfo(address);
  res.json(info);
});

app.post('/transaction', (req, res) => {
  let transactionData = req.body;
  let transaction = Transaction.fromObject(transactionData, 'hex');
  // TODO add to pending transactions,
  // TODO broadcast to other peer nodes.
  addTransaction(transaction, (err) => {
    if (err) {
      res.status(401);
      return res.json(err.message);
    }
    node.broadCast({
      type: 'transaction',
      data: transaction.toObject('hex'),
    });
    return res.json(transaction.hash.toString('hex'));
  });
});

app.use('*', (req, res) => {
  res.sendStatus(404);
});

export default app;
