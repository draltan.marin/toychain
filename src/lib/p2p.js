import WebSocket from 'ws';
import defaultLogger from './voidLogger';

class Node {
  constructor(
    handler = () => {},
    welcome = () => null,
    logger = defaultLogger,
  ){
    this.connections = {};
    this.handler = handler;
    this.welcome = welcome;
    this.logger = logger;
  }

  // server for incomming connections
  start(options = {}) {
    let {
      host = '127.0.0.1',
      port,
      peers = [],
    } = options;
    this.host = host;
    this.port = port;

    return new Promise((resolve, reject) => {
      this.server = new WebSocket.Server({port: this.port});

      this.server.on('listening', () => {
        resolve(this);
      });

      this.server.on('connection', (connection, req) => {
        let from = req.headers['origin'];
        this.addConnection(connection, from, from);
      });

      this.server.on('error', error => {
        reject(new Error(`problem on server ${this.address()}, ${error}`));
      });

      peers.forEach(peer => this.addPeer(peer));
    });
  }

  // be client for a given peer
  addPeer(peer) {
    if (peer === this.address()) {
      return;
    }
    if (this.connections[peer] === undefined) {
      // console.log(`initializing connection to ${peer}`);
      let connection = new WebSocket(peer, {origin: this.address()});
      connection.on('open', () => {
        this.addConnection(connection, peer, this.address());
      });
      connection.on('error', () => {
        // peer is not responding, ignore
        this.logger.warn(`${this.address()}: peer ${peer} is not responding`);
      });
    }
  }

  address() {
    return `ws://${this.host}:${this.port}`;
  }

  // init a connection (either as server or as peer)
  addConnection(connection, id, initializedBy) {
    let welcomeMessage = this.welcome();
    if (welcomeMessage) {
      connection.send(JSON.stringify(welcomeMessage));
    }

    let messageHandlerEvent = (data) => { this.handler(JSON.parse(data), id); };
    let removeConnectionEvent = () => this.removeConnection(id);

    connection.on('message', messageHandlerEvent);
    connection.on('close', removeConnectionEvent);
    connection.on('error', removeConnectionEvent);

    let existing = this.connections[id];
    if (existing) {
      // console.log(`${this.address()}: existing connection with ${id}\n\tinitializedBy ${this.connections[id].initializedBy}\n\tattempting initializedBy ${initializedBy}`)

      let toBeKept, toBeDropped;
      if (existing.initializedBy > initializedBy) {
        toBeKept = existing;
        toBeDropped = {connection, initializedBy, removeConnectionEvent, messageHandlerEvent};
      } else {
        toBeKept = {connection, initializedBy, removeConnectionEvent, messageHandlerEvent};
        toBeDropped = existing;
        this.logger.info(`${this.address()}: switch connection to ${id} to the one initialized by ${initializedBy}`);
      }
      // removeEvents
      toBeDropped.connection.removeEventListener('close', toBeDropped.removeConnectionEvent);
      toBeDropped.connection.removeEventListener('error', toBeDropped.removeConnectionEvent);
      toBeDropped.connection.removeEventListener('message', toBeDropped.messageHandlerEvent);

      // to be terminated by initializer
      if (toBeDropped.initializedBy === this.address()){
        try {
          toBeDropped.connection.terminate();
        } catch (error) {
          this.logger.warn(`${this.address()}: error disconnecting ${id}`);
        }
      }
      this.connections[id] = toBeKept;
    } else {
      this.logger.info(`${this.address()}: connected to ${id} initializedBy ${initializedBy}`);
      this.connections[id] = {
        connection,
        initializedBy,
        removeConnectionEvent,
        messageHandlerEvent,
      };
    }
  }

  // close a connection (either as server or as peer)
  removeConnection(connection) {
    delete this.connections[connection];
  }

  disconnect(peer) {
    let {
      connection,
      removeConnectionEvent,
      messageHandlerEvent,
    } = this.connections[peer] || {};
    if (connection) {
      this.logger.info(`${this.address()}: disconnecting ${peer}`);
      connection.removeEventListener('close', removeConnectionEvent);
      connection.removeEventListener('error', removeConnectionEvent);
      connection.removeEventListener('message', messageHandlerEvent);
      try {
        connection.terminate();
      } catch (error) {
        this.logger.warn(`${this.address()}: error disconnecting ${peer}`);
      }
      this.removeConnection(peer);
    }
  }
  // broadcast a message to all connected peers
  broadCast(message) {
    Object.keys(this.connections).forEach(peer => {
      let {connection} = this.connections[peer];
      connection.send(JSON.stringify(message));
    });
  }

  to(peer, message) {
    let {connection} = this.connections[peer];
    connection.send(JSON.stringify(message));
  }
}

export default Node;
