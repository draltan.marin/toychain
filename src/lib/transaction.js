import crypto from 'crypto';

import {numToBuffer, utf8} from './util';

import {
  sign,
  verify,
  getPublic,
} from './keys';

// from, to : Buffer
// value : number
class Transaction {
  constructor({
    number,
    from,
    to,
    value,
    payload = ''
  }, key) {
    this.number = number;
    this.from = Buffer.from(from);
    this.to = Buffer.from(to);
    this.value = value;
    this.payload = Buffer.from(payload);
    this.updateSignature(key);
    this.hash = this.computeHash();
  }

  setPayload(payload = '', key) {
    this.payload = Buffer.from(payload);
    this.updateSignature(key);
    this.hash = this.computeHash();
  }

  setValue(value, key) {
    this.value = value;
    this.updateSignature(key);
    this.hash = this.computeHash();
  }

  setFrom(from, key) {
    this.from = Buffer.from(from);
    this.updateSignature(key);
    this.hash = this.computeHash();
  }

  setTo(to, key) {
    this.to = Buffer.from(to);
    this.updateSignature(key);
    this.hash = this.computeHash();
  }

  setTransactionNumber(number, key) {
    this.number = number;
    this.updateSignature(key);
    this.hash = this.computeHash();
  }

  getMiningData() {
    return Buffer.concat([
      numToBuffer(this.number),
      this.from,
      this.to,
      numToBuffer(this.value),
      this.payload,
      this.signature,
    ]);
  }

  getSignData() {
    return crypto.createHash('sha256')
      .update(numToBuffer(this.number))
      .update(this.from)
      .update(this.to)
      .update(numToBuffer(this.value))
      .update(this.payload)
      .digest();
  }

  computeHash() {
    return crypto.createHash('sha256')
      .update(this.getMiningData())
      .digest();
  }

  sign(key){
    if (!key) {
      return;
    }
    // expect private key to match from address
    if (Buffer.compare(getPublic(key), this.from) !== 0) {
      throw new Error('invalid key');
    }
    this.signature = sign(this.getSignData(), key);
    this.hash = this.computeHash();
  }

  updateSignature(key) {
    this.signature = Buffer.alloc(0);
    this.sign(key);
  }

  isSigned() {
    return this.signature.length !== 0;
  }

  verify() {
    if (!this.isSigned()) {
      return false;
    }
    return verify(
      this.getSignData(),
      this.signature,
      this.from
    );
  }

  static fromObject({
    number,
    from = '',
    to = '',
    value,
    payload = '',
    signature = '',
  }, encoding = 'hex') {
    let transaction = new Transaction({
      number,
      from: Buffer.from(from, encoding),
      to: Buffer.from(to, encoding),
      value,
      payload: Buffer.from(payload, 'utf8'),
    });
    transaction.signature = Buffer.from(signature, encoding);
    transaction.hash = transaction.computeHash();
    return transaction;
  }

  toObject(encoding = 'hex') {
    return {
      number: this.number,
      from: this.from.toString(encoding),
      to: this.to.toString(encoding),
      value: this.value,
      payload: utf8(this.payload),
      signature: this.signature? this.signature.toString(encoding): '',
      hash: this.hash.toString(encoding)
    };
  }

  toString(encoding = 'hex'){
    return JSON.stringify(this.toObject(encoding));
  }
}

export default Transaction;
